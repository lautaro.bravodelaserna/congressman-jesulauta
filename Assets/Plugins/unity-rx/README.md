[![Name Badge][name]][name-link]
[![Version Badge][version]][version-link]

[![Pipeline Badge][pipeline]][pipeline-link]
[![Open Issues Badge][open-issues]][open-issues-link]
[![Open Merge Requests Badge][open-merge-requests]][open-merge-requests-link]

[![Slack Badge][slack]][slack-link]

[name]: https://badges.etermax.com/packageJson?key=name&label=name&color=yellow&project=unity-tools/unity-rx
[name-link]: package.json
[version]: https://badges.etermax.com/packageJson?key=version&label=version&project=unity-tools/unity-rx
[version-link]: package.json
[pipeline]: https://badges.etermax.com/pipeline?project=unity-tools/unity-rx
[pipeline-link]: https://gitlab.etermax.net/unity-tools/unity-rx/pipelines
[open-issues]: https://badges.etermax.com/open-issues?project=unity-tools/unity-rx
[open-issues-link]: https://gitlab.etermax.net/unity-tools/unity-rx/issues?scope=all&utf8=✓&state=opened
[open-merge-requests]: https://badges.etermax.com/open-merge-requests?project=unity-tools/unity-rx
[open-merge-requests-link]: https://gitlab.etermax.net/unity-tools/unity-rx/merge_requests
[slack]: https://badges.etermax.com/slack?channel=ask-platform-team
[slack-link]: https://etermax.slack.com/app_redirect?channel=ask-platform-team

# Unity Rx

# Description

This package contains the UniRx library for functional programming in Unity.

# Prerequisites

Add the dependency to your `package.json`.

```json
"dependencies": {
  "@etermax/unity-rx": "VERSION"
}
```

# Usage

For more information go to the UniRx Official Github

https://github.com/neuecc/UniRx

# Dependencies

No dependencies needed.
