## [7.1.0]

### Fixed

- Fix AsyncReactiveCommand constructor #365, thanks @TORISOUP
- Fix Build error of UniRx.Example
- Fix await behaviour of ReactiveProperty #419, thanks @TORISOUP
- async/await is separated to https://github.com/Cysharp/UniTask , if error is occured, remove package(clean) and re-import.

## [7.0.0]

### Changed

- UniRx.Async is separated to https://github.com/Cysharp/UniTask

## [6.2.2]

### Changed

- Add CancelletaionToken.RegisterWithoutCaptureExecutionContext
- Fix warning about WWW on Unity 2018.3
- Fix does not work UniRx.Async on Unity 2018.3
- More info https://github.com/neuecc/UniRx/releases

## [6.2.0]

### Changed

- Improve cancellation/exception handling.
- All unhandled exception was routed to `UniTaskScheduler.UnobservedTaskException`(called `UniTask/UniTaskVoid.Forget` or when unhandled task was GCed)
- `OperationCanceledException` can modify to `AwaiterStatus.Canceled` in async method and can ignore when routed to top.
- Add `UniTask.Run`
- Add `UniTask.SwitchToSynchronizationContext`
- Add `UniTask.Lazy`
- Add `UniTask.Void`
- Add `UniTask.SwitchToMainThread`
- Add `UniTask.SuppressCancellationThrow`
- Add `CancellationTokenSource.CancelAfterSlim`
- Add `CancellationTokenSource.RegisterRaiseCancelOnDestroy`
- Add `Progress.CreateOnlyValueChanged`
- Add `UniTaskScheduler(UnobservedTaskException/PropagateOperationCanceledException/UnobservedExceptionWriteLogType/DispatchUnityMainThread)`
- Add `Task.AsUniTask(bool useCurrentSynchronizationContext)`
- Add await support to `ValueTuple`
- Add `UniRx -> UniTaskTracker` editor window
- Add `Dropdown.OnValueChangedAsync`
- Add `Dropdown.OnValueChangedAsObservable`
- Add `StackTrace.CleanupAsyncStackTrace`
- Add `Exception.CleanupAsyncStackTrace`
- Add `ReactiveProperty.WaitUntilValueChangedAsync(CancellationToken)`
- Add `ReactiveCommand.WaitUntilExecuteAsync(CancellationToken)`
- Add `AwaiterStatus.IsCompleted/IsCompletedSuccessfully/IsCanceled/IsFaulted` extension methods
- Fix `UniTask.SwitchToThreadPool` does not yield correctly
- Fix `UniTask.SwitchToTaskPool` does not yield correctly
- Fix `IEnumerable[UniTask]` shortcut #336, thanks @Reqweldzen
- Fix Propagate `Canceled` status when throws `OperationCanceledException`
- Improve AsyncTriggers throws `OperationCanceledException` when trigger was destroyed
- Improve unhandled exception was routed to `UniTaskScheduler.UnobservedTaskException` when unahandled task was GCed
- Improve peformance `UniTask.WhenAll`
- Improve performance `UniTask.Yield`
- Improve reduce AsyncStateMachine garbage
- Improve `UniTask.ToString` shows status
- Breaking Changes `UniTask.WithCancellation` is removed(use `SuppressCancellationThrow` instead)
- Internaly Unit test moved to Unity Test Runner

## [6.1.2]

### Changed

- Fix compile error in C# 6.0
- Improve performance Task.WhenAll
- Improve performance UniTask.Timeout
- Add UniTask[UniTask[]].Unwrap
- Add IEnumerable[UniTask].GetAwaiter(UniTask.WhenALl shortcut)
- Add WithCancellationWithoutException
- Add UniTask.WithIsCancled
- Breaking Change Add IAwaiter.AwaiterStatus
