The format is based on Keep a Changelog and this project adheres to Semantic Versioning.

## [8.3.0]
### Added

-  ReturnObservableOf Object & Exception for Substitutes.

## [8.2.0]
### Added

-  ButtonExtensions with ThrottleFirst

## [8.1.2]
### Fixed

-  Added System usings for .NET 4 support.

## [8.1.1]
### Fixed

-  AnimationObservable when adding AnimationTrigger the second time.

## [8.1.0]
### Added

-  PlayAsObservable extension.

### Changed

- [Rodrigo Rearden MR] SelectMaybe now calls the function parameter only once on each OnNext.

## [8.0.0]
### Removed

-  Embeded Functional.

### Added 

- Functional as dependency

## [7.1.0]
### Changed

-  UniRx version to 7.1.0

## [7.0.0-d]
### Added

-  Test().AssertNoValue assert onNext is never called.

## [7.0.0-c]
### Changed

-  SelectMaybeObservable completes when has no value.

## [7.0.0-b]
### Added

-  DequeuMaybe()  extension method for queues

## [7.0.0]

### Changed

- Version matching with UniRx version.

## [1.0.0]

### Added

- UniRx custom extensions.

- Maybe source code.
