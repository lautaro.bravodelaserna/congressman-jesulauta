# Unity ReactiveX

## Description

This package is a wrapper of UniRx providing useful extensions.

# Prerequisites

Set the dependency in your `package.json`

```json
"@etermax/unity-reactivex": "VERSION"
```

# Usage

## Test

- AssertValue

```csharp
var message = "Hello World";
Observable
    .Return(message)
    .Test()
    .AssertValue(message);
```

```csharp
var message = "Hello World";
Observable
    .Return(message)
    .Test()
    .AssertValue(value => value == message);
```

- AssertComplete

```csharp
Observable
    .ReturnUnit()
    .Test()
    .AssertComplete();
```

- AssertNotCompleted

```csharp
Observable
    .Throw<string>(new Exception())
    .Test()
    .AssertNotCompleted();
```

- AssertException

```csharp
var error = new WebException();
Observable
    .Throw<string>(error)
    .Test()
    .AssertException(error);
```

- AssertError

```csharp
Observable
    .Throw<string>(new WebException())
    .Test()
    .AssertError<WebException>();
```

- AssertNoErrors

```csharp
Observable
    .ReturnUnit()
    .Test()
    .AssertNoErrors();
```

- AssertNoOnNext

```csharp
Observable
    .Empty<string>()
    .Test()
    .AssertNoOnNext();
```

- ReturnsObservableOf

When you want to make an observable return some value for testing you can use:

```csharp
myService.Get().ReturnsObservableOf(value)
myService.Get().ReturnsObservableOf(exception)
```

## FromAction

Will execute the block and will return Unit to the next Observable. If any error is throw inside the block it will be propagated to the stream.

```csharp
CreateObservable
    .FromAction(() => DoSomething());
```

## FromFunction

Will execute the block and forward the value to the next Observable. If any error is throw inside the block it will be propagated to the stream.

```csharp
CreateObservable
    .FromFunction(() => 5)
```

## SelectMaybe

```csharp
Observable.Return(new List<string>())
.SelectMaybe(list => list.FirstMaybe())
.SelectMany(paymentMethod =>{
    //This code is not executed if firstMaybe is empty.
};
```

## SelectWhere

Example:

If entry value is type of `Event` will transform to `SomeEvent`

```csharp
var eventMessage = "Hello World!";
Event myEvent = new SomeEvent(eventMessage);
Observable
    .Return(myEvent)
    .SelectWhere<Event, SomeEvent>()
    .Subscribe(@event => Debug.Log(@event.Message));

// Output: "Hello World!"
```

## SwitchIfError

```csharp
var message = "Hello World!";
Observable
    .Throw<string>(new Exception())
    .SwitchIfError(() => Observable.Return(message))
    .Subscribe(value => Debug.Log(value))

// Output: "Hello World!"
```

## CatchErrorWhere

```csharp
var message = "Hello World!";
Observable
    .Throw<string>(new TimeoutException())
    .CatchErrorWhere(e => e is TimeoutException, () => Observable.Return(message))
    .Subscribe(value => Debug.Log(value));

// Output: "Hello World!"
```

## SwitchIfEmpty

```csharp
var message = "Hello World!";
Observable
    .Empty<string>()
    .SwitchIfEmpty(() => Observable.Return(message))
    .Subscribe(value => Debug.Log(message))

// Output: "Hello World!"
```

## Dependencies

```json
"@etermax/nsubstitute": "2.x",
"@etermax/unity-rx": "7.x",
"@etermax/functional": "1.x"
```
