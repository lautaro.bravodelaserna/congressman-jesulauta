using UniRx;
using System;
using UnityEngine;

namespace Etermax.UnityReactiveX.Observables
{
    public class ObservableAnimationTrigger : MonoBehaviour
    {
        private readonly Subject<Unit> _subject = new Subject<Unit>();
    
        public IObservable<Unit> OnAnimationEnd()
        {
            return Observable.Create<Unit>(emitter =>
            {
                _subject.Subscribe(_ =>
                {
                    emitter.OnNext(_);
                    emitter.OnCompleted();
                }, emitter.OnError, emitter.OnCompleted);

                return Disposable.Empty;
            });
        }

        public void RxEnd()
        {
            if (_subject != null)
            {
                _subject.OnNext(Unit.Default);
            }
        }

        private void OnDestroy()
        {
            if (_subject != null) _subject.OnCompleted();
        }
    }
}