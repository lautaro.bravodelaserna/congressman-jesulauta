using System;
using UniRx;

namespace Etermax.UnityReactiveX.Observables
{
	public class SwitchIfErrorObservable<T> : IObservable<T>
	{
		private readonly IObservable<T> source;
		private readonly Func<IObservable<T>> other;

		public SwitchIfErrorObservable(IObservable<T> source, Func<IObservable<T>> other)
		{
			this.source = source;
			this.other = other;
		}

		public IDisposable Subscribe(IObserver<T> observer)
		{
			return source.Subscribe(
				value =>
				{
					observer.OnNext(value);
					observer.OnCompleted();
				}
				, exception => other.Invoke().Subscribe(observer.OnNext, observer.OnError, observer.OnCompleted)
				, observer.OnCompleted);
		}
	}
}