using System;
using System.Linq;
using UniRx;
using UnityEngine;

namespace Etermax.UnityReactiveX.Observables
{
    public class AnimationObservable : IObservable<Unit>
    {
        private readonly Animation _animation;
        private readonly AnimationClip _animationClip;
        private readonly ObservableAnimationTrigger _animationTrigger;
    
        public AnimationObservable(AnimationClip animationClip, Animation sourceAnimation)
        {
            _animationClip = animationClip;
            _animation = sourceAnimation;
            _animationTrigger = sourceAnimation.gameObject.GetComponent<ObservableAnimationTrigger>();
            if(_animationTrigger == null)
                _animationTrigger = sourceAnimation.gameObject.AddComponent<ObservableAnimationTrigger>();
        
            if (animationClip.events.Any(animEvent => animEvent.functionName == "RxEnd"))
            {
         
            }
            else
            {
                var animationEvent = new AnimationEvent
                {
                    time = animationClip.length,
                    functionName = "RxEnd"
                };
            
                animationClip.AddEvent(animationEvent);
            }
        }

        public IDisposable Subscribe(IObserver<Unit> observer)
        {
            _animation.Stop();
            _animation.Play(_animationClip.name);
            return _animationTrigger
                .OnAnimationEnd()
                .Subscribe(observer.OnNext, observer.OnError, observer.OnCompleted);
        }
    }
}