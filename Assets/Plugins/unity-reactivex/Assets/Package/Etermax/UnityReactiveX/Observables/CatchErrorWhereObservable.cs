using System;
using UniRx;

namespace Etermax.UnityReactiveX.Observables
{
	public class CatchErrorWhereObservable<T> : IObservable<T>
	{
		private readonly IObservable<T> source;
		private readonly Func<IObservable<T>> other;
		private readonly Func<Exception, bool> predicate;
		private bool hasValue;

		public CatchErrorWhereObservable(IObservable<T> source, Func<IObservable<T>> other,
			Func<Exception, bool> predicate)
		{
			this.source = source;
			this.other = other;
			this.predicate = predicate;
		}

		public IDisposable Subscribe(IObserver<T> observer)
		{
			return source.Subscribe(
				value =>
				{
					observer.OnNext(value);
					observer.OnCompleted();
				}
				, exception =>
				{
					if (predicate(exception))
					{
						other.Invoke().Subscribe(observer.OnNext, observer.OnError, observer.OnCompleted);
					}
					else
					{
						observer.OnError(exception);
					}
				}, observer.OnCompleted);
		}
	}
}