using System;
using UniRx;

namespace Etermax.UnityReactiveX.Observables
{
	public class SelectErrorObservable<T> : IObservable<T>
	{
		private readonly IObservable<T> source;
		private readonly Func<Exception, Exception> _transform;

		public SelectErrorObservable(IObservable<T> source, Func<Exception, Exception> transform)
		{
			this.source = source;
			_transform = transform;
		}

		public IDisposable Subscribe(IObserver<T> observer)
		{
			return source.Subscribe(
				observer.OnNext
				, error => observer.OnError(_transform(error))
				, observer.OnCompleted);
		}
	}
}