using System;
using Functional.Maybe;
using UniRx;

namespace Etermax.UnityReactiveX.Observables
{
	public class SelectMaybeObservable<T, U> : IObservable<U>
	{
		private readonly IObservable<T> _source;
		private readonly Func<T, Maybe<U>> _other;

		public SelectMaybeObservable(IObservable<T> source, Func<T, Maybe<U>> other)
		{
			_source = source;
			_other = other;
		}

		public IDisposable Subscribe(IObserver<U> observer)
		{
			return _source.Subscribe(value =>
				_other(value)
					.Do(observer.OnNext)
					.DoWhenAbsent(observer.OnCompleted),
				observer.OnError,
				observer.OnCompleted
			);
		}
	}
	
	class MaybeNullValueException : Exception
	{
		public MaybeNullValueException() : base("Value cannot be null")
		{
		}
	}
}