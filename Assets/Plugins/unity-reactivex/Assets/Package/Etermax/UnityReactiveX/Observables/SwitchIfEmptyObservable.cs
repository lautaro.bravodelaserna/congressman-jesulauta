using System;
using UniRx;

namespace Etermax.UnityReactiveX.Observables
{
	class SwitchIfEmptyObservable<T> : IObservable<T>
	{
		private readonly IObservable<T> source;
		private readonly Func<IObservable<T>> other;
		private bool hasValue;

		public SwitchIfEmptyObservable(IObservable<T> source, Func<IObservable<T>> other)
		{
			this.source = source;
			this.other = other;
		}

		public IDisposable Subscribe(IObserver<T> observer)
		{
			return source.Subscribe(
				value =>
				{
					hasValue = true;
					observer.OnNext(value);
					observer.OnCompleted();
				}
				, exception =>
				{
					if (exception is MaybeNullValueException)
						other.Invoke().Subscribe(observer.OnNext, observer.OnError, observer.OnCompleted);
					else
					{
						observer.OnError(exception);
					}
				}
				, () =>
				{
					if (!hasValue)
					{
						other.Invoke().Subscribe(observer.OnNext, observer.OnError, observer.OnCompleted);
					}
				});
		}
	}
}