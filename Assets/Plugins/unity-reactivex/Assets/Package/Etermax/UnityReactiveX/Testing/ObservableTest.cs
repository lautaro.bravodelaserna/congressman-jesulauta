using System;
using NSubstitute;
using UniRx;

namespace Etermax.UnityReactiveX.Testing
{
	public class ObservableTest<T>
	{
		private readonly IObserver<T> _testObserver;

		public ObservableTest(IObservable<T> sourceObservable)
		{
			_testObserver = Substitute.For<IObserver<T>>();
			sourceObservable.Subscribe(_testObserver);
		}

		public ObservableTest<T> AssertValue(T value)
		{
			_testObserver.Received().OnNext(value);
			return this;
		}
		
		public ObservableTest<T> AssertNoValue()
		{
			_testObserver.Received(0).OnNext(Arg.Any<T>());
			return this;
		}

		public ObservableTest<T> AssertValue(Predicate<T> predicate)
		{
			_testObserver.Received().OnNext(Arg.Is<T>(value => predicate(value)));
			return this;
		}

		public ObservableTest<T> AssertComplete()
		{
			_testObserver.Received().OnCompleted();
			return this;
		}

		public ObservableTest<T> AssertException(Exception e)
		{
			_testObserver.Received().OnError(e);
			return this;
		}

		public ObservableTest<T> AssertNoErrors()
		{
			_testObserver.DidNotReceive().OnError(Arg.Any<Exception>());
			return this;
		}

		public ObservableTest<T> AssertNotCompleted()
		{
			_testObserver.DidNotReceive().OnCompleted();
			return this;
		}

		public ObservableTest<T> AssertError()
		{
			_testObserver.Received().OnError(Arg.Any<Exception>());
			return this;
		}

		public ObservableTest<T> AssertError<U>() where U : Exception
		{
			_testObserver.Received().OnError(Arg.Is<Exception>(error => error is U));
			return this;
		}

		public ObservableTest<T> AssertNoOnNext()
		{
			_testObserver.DidNotReceive().OnNext(Arg.Any<T>());
			return this;
		}
	}
}