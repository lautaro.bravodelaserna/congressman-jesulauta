using UniRx;
using System;
using NSubstitute;

namespace Etermax.UnityReactiveX.Testing
{
	public static class UnityReactiveXTestingExtensions
	{
		public static ObservableTest<T> Test<T>(this IObservable<T> observable)
		{
			return new ObservableTest<T>(observable);
		}
		
		public static void ReturnsObservableOf<T>(this IObservable<T> observable, T expected = default(T))
		{
			var a = Observable.Return(expected);
			observable.Returns(a);
		}
        
		public static void ReturnsObservableOf<T>(this IObservable<T> observable, Exception expected)
		{
			var a = Observable.Throw<T>(expected);
			observable.Returns(a);
		}
	}
}