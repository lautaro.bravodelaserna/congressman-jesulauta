using System;
using UniRx;

namespace Etermax.UnityReactiveX
{
	public static class CreateObservable
	{
		public static IObservable<Unit> FromAction(Action action)
		{
			return Observable.Create<Unit>(emitter =>
			{
				try
				{
					action.Invoke();
					emitter.OnNext(Unit.Default);
					emitter.OnCompleted();
				}
				catch (Exception exception)
				{
					emitter.OnError(exception);
				}

				return Disposable.Empty;
			});
		}
		
		public static IObservable<T> FromFunction<T>(Func<T> function)
		{
			return Observable.Create<T>(emitter =>
			{
				try
				{
					var value = function.Invoke();
					emitter.OnNext(value);
					emitter.OnCompleted();
				}
				catch (Exception exception)
				{
					emitter.OnError(exception);
				}

				return Disposable.Empty;
			});
		}
	}
}