using System;
using UniRx;
using UnityEngine.UI;

namespace Etermax.UnityReactiveX
{
    public static class ButtonExtensions
    {
        public static IObservable<Unit> OnClickThrottleFirst(this Button button)
        {
            return button.OnClickAsObservable().ThrottleFirst(TimeSpan.FromSeconds(1));
        }

        public static IObservable<Unit> OnClickThrottleFirst(this Button button, float seconds)
        {
            return button.OnClickAsObservable().ThrottleFirst(TimeSpan.FromSeconds(seconds));
        }

        public static IObservable<Unit> OnClickThrottleFirst(this Button button, TimeSpan timeSpan)
        {
            return button.OnClickAsObservable().ThrottleFirst(timeSpan);
        }
    }
}