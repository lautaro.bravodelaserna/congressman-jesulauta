using System;
using Etermax.UnityReactiveX.Observables;
using Functional.Maybe;
using UniRx;
using UnityEngine;

namespace Etermax.UnityReactiveX
{
	public static class UnityRxExtensions
	{
		public static IObservable<TResult> SelectWhere<TSource, TResult>(this IObservable<TSource> source)
			where TResult : TSource
		{
			return source.Where(@object => @object is TResult).Select(@object => (TResult) @object);
		}

		public static IObservable<TSource> ConcatObservables<TSource>(params IObservable<TSource>[] source)
		{
			return source.Concat();
		}

		public static IObservable<T> SwitchIfEmpty<T>(this IObservable<T> source,
			Func<IObservable<T>> other)
		{
			return new SwitchIfEmptyObservable<T>(source, other);
		}

		public static IObservable<T> SwitchIfError<T>(this IObservable<T> source,
			Func<IObservable<T>> other)
		{
			return new SwitchIfErrorObservable<T>(source, other);
		}

		public static IObservable<T> SelectError<T>(this IObservable<T> source,
			Func<Exception, Exception> transform)
		{
			return new SelectErrorObservable<T>(source, transform);
		}

		public static IObservable<T> CatchErrorWhere<T>(this IObservable<T> source,
			Func<Exception, bool> predicate, Func<IObservable<T>> other)
		{
			return new CatchErrorWhereObservable<T>(source, other, predicate);
		}

		public static IObservable<U> SelectMaybe<T, U>(this IObservable<T> source, Func<T, Maybe<U>> other)
		{
			return new SelectMaybeObservable<T, U>(source, other);
		}
		
		public static IObservable<Unit> PlayAsObservable(this Animation source, string clipName)
		{
			var observable = Observable.ReturnUnit();
			foreach (AnimationState animationState in source)
			{
				if (animationState.clip.name != clipName) continue;
				observable = new AnimationObservable(animationState.clip, source);
				break;
			}

			return observable;
		}
	}
}