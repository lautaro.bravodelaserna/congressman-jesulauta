# unirx-test-scheduler

Adapted VirtualTimeScheduler` and `TestScheduler classes from https://github.com/Reactive-Extensions/Rx.NET,` basically adapting them to UniRx's Scheduler with no TState.

Original source-code from https://github.com/neuecc/UniRx/pull/220.


## Usage

## Notes

- Replaces ocurrences of `nameof` (not available in C#4.0) on throw messages