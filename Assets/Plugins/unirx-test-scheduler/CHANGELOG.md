## [1.0.3]
### Added
- Assembly definitions

## [1.0.1]
### Fixed
- Fix compile errors using the new version of UniRx (6.1.2)