using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;

public class AnxiousCongressman : CongressmanBase
{
    public override IObservable<VoteType> Vote(string law)
    {
        var dic = new Dictionary<VoteType, Func<string, bool>>
        {
            {VoteType.Abstain, s => s == ""},
            {VoteType.Approved, s => s.Length > 10},
            {VoteType.Denied, s => s.Length > 0 && s.Length <=10},
        };

        return Observable.ReturnUnit()
            .Delay(TimeSpan.FromMilliseconds(law.Length * 100))
            .Timeout(TimeSpan.FromMilliseconds(300))
            .Select(_ => dic.First(d => d.Value(law)).Key)
            .Catch<VoteType, TimeoutException>(RejectLaw);
    }

    IObservable<VoteType> RejectLaw(TimeoutException arg)
    {
        return Observable.Return(VoteType.Denied);
    }
}