using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

public class CongressmanCamera
{
    public IObservable<LawStatement> Vote(CongressmanBase[] congressmans, string law)
    {
        return congressmans.ToObservable()
            .SelectMany(_ => _.Vote(law))
            .Aggregate(new List<VoteType>(), (l, v) =>
            {
                l.Add(v);
                return l;
            })
            .Select(vs =>
            {
                if (vs.Count(v => v == VoteType.Approved) > vs.Count / 2)
                    return LawStatement.Approved;
                return LawStatement.Denied;
            })
            .Catch<LawStatement, BoicotException>(RejectLaw);
    }

    IObservable<LawStatement> RejectLaw(BoicotException arg)
    {
        return Observable.Return(LawStatement.Denied);
    }
}

public class CongressmanCameraView : MonoBehaviour
{
    private void Start()
    {
        var congressmans = new CongressmanBase[]
        {
            new Congressman(),
            new AnxiousCongressman(),
            new MillhouseCongressman(),
            new CometaCongressman(true),
            new BoicotCongressman(),
        };
        var camera = new CongressmanCamera();
        camera.Vote(congressmans, "ALTA LAW PAPA");
        CongressmanBase.eventBus.Subscribe(info => PublishMessage(info.Id + ";" + info.CongressmanType + info.Vote));
    }

    public void PublishMessage(string text)
    {
        Debug.Log(text);
    }
}


public enum LawStatement
{
    None,
    Denied,
    Approved,
}