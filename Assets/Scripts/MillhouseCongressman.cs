using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;

public class MillhouseCongressman : CongressmanBase
{
    public override IObservable<VoteType> Vote(string law)
    {
        var dic = new Dictionary<VoteType, Func<string, bool>>
        {
            {VoteType.Abstain, s => s == ""},
            {VoteType.Approved, s => s.Length > 10},
            {VoteType.Denied, s => s.Length > 0 && s.Length <=10},
        };

        return Observable.ReturnUnit()
            .Delay(TimeSpan.FromMilliseconds(law.Length * 100))
            .Delay(TimeSpan.FromMilliseconds(500))
            .Select(_ => dic.First(d => d.Value(law)).Key);
    }
}