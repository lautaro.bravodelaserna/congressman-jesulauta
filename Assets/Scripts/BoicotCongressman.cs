using System;
using UniRx;

public class BoicotCongressman : CongressmanBase
{
    public override IObservable<VoteType> Vote(string law)
    {
        
        return Observable.Throw<VoteType>(new BoicotException());
    }

   
}