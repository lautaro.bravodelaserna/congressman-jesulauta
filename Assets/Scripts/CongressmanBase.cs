using System;
using UniRx;

public abstract class CongressmanBase
{
    public static Subject<CongressmanInfo> eventBus = new Subject<CongressmanInfo>();
    public abstract IObservable<VoteType> Vote(string law);
}

public class CongressmanInfo
{
    public string Id;
    public string CongressmanType;
    public VoteType Vote;

    public CongressmanInfo(string id, string congressmanType, VoteType vote)
    {
        Id = id;
        CongressmanType = congressmanType;
        Vote = vote;
    }
}