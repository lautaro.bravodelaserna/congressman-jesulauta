﻿using Functional.Maybe;
using NUnit.Framework;
using UniRx;

namespace Editor
{
    public class CongressmanTest
    {
        private Congressman _congressman;
        private TestScheduler _testScheduler;

        [SetUp]
        public void SetUp()
        {
            _testScheduler = new TestScheduler();
            Scheduler.DefaultSchedulers.TimeBasedOperations = _testScheduler;
            _congressman = new Congressman();
        }

        [TestCase("long answer", VoteType.Approved)]
        [TestCase("short", VoteType.Denied)]
        [TestCase("", VoteType.Abstain)]
        public void get_vote_when_vote(string law, VoteType expected)
        {
            var vote = WhenVoting(law);

            ThenVoteIsEqualTo(vote.Value, expected);
        }

        Maybe<VoteType> WhenVoting(string law)
        {
            Maybe<VoteType> vote = default;
            _congressman.Vote(law).Subscribe(currentVote => vote = currentVote.ToMaybe());
            _testScheduler.Start();
            return vote;
        }

        private void ThenVoteIsEqualTo(VoteType current, VoteType expected)
        {
            Assert.AreEqual(expected, current);
        }
    }
}
