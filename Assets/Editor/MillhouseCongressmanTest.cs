using System;
using Functional.Maybe;
using NUnit.Framework;
using UniRx;

namespace Editor
{
    public class MillhouseCongressmanTest
    {
        private MillhouseCongressman _congressman;
        private TestScheduler _testScheduler;

        [SetUp]
        public void SetUp()
        {
            _testScheduler = new TestScheduler();
            Scheduler.DefaultSchedulers.TimeBasedOperations = _testScheduler;
            _congressman = new MillhouseCongressman();
        }

        [TestCase("long answer", VoteType.Approved)]
        [TestCase("short", VoteType.Denied)]
        [TestCase("", VoteType.Abstain)]
        public void get_vote_when_vote_fails(string law, VoteType expected)
        {
            var vote = WhenVotingShort(law);

            Assert.IsTrue(vote.IsNothing());
        }
        
        [TestCase("long answer", VoteType.Approved)]
        [TestCase("short", VoteType.Denied)]
        [TestCase("", VoteType.Abstain)]
        public void get_vote_when_vote_succeeds(string law, VoteType expected)
        {
            var vote = WhenVoting(law);

            ThenVoteIsEqualTo(vote.Value, expected);
        }

        Maybe<VoteType> WhenVotingShort(string law)
        {
            Maybe<VoteType> vote = default;
            _congressman.Vote(law).Subscribe(currentVote => vote = currentVote.ToMaybe());
            _testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(law.Length * 100).Ticks + 1);
            return vote;
        }
        
        Maybe<VoteType> WhenVoting(string law)
        {
            Maybe<VoteType> vote = default;
            _congressman.Vote(law).Subscribe(currentVote => vote = currentVote.ToMaybe());
            _testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(law.Length * 100).Ticks + 1);
            _testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(500).Ticks);
            return vote;
        }

        private void ThenVoteIsEqualTo(VoteType current, VoteType expected)
        {
            Assert.AreEqual(expected, current);
        }
    }
}