using NUnit.Framework;
using UniRx;

namespace Editor
{
    public class BoicotCongressmanTest
    {
        BoicotCongressman boicotCongressman;

        [SetUp]
        public void SetUp()
        {
            boicotCongressman = new BoicotCongressman();
        }

        [TestCase("long answer")]
        [TestCase("short")]
        [TestCase("")]
        public void test_boicot(string law)
        {
            Assert.Throws<BoicotException>(() => WhenVoting(law));
        }

        void WhenVoting(string law)
        {
            boicotCongressman.Vote(law).Subscribe();
        }
    }
}