using Functional.Maybe;
using NUnit.Framework;
using UniRx;

namespace Editor
{
    public class CongressmanCameraTest
    {
        private const bool COMETA = true;
        private CongressmanCamera _congressmanCamera;
        TestScheduler testScheduler;
        
        [SetUp]
        public void SetUp()
        {
            testScheduler = new TestScheduler();
            Scheduler.DefaultSchedulers.TimeBasedOperations = testScheduler;
            _congressmanCamera = new CongressmanCamera();
        }
        
        [TestCase("long answer", LawStatement.Approved)]
        [TestCase("short", LawStatement.Denied)]
        public void vote_for_law_by_a_group_of_congressman(string law, LawStatement expected)
        {
            CongressmanBase[] congressmans = {
                new Congressman(),
                new Congressman(),
                new Congressman(),
            };
            var lawStatement = WhenVoting(congressmans, law);
            
            ThenLawStatementIsEqualTo(lawStatement.Value, expected);
        }

        [TestCase("long aswer")]
        [TestCase("short")]
        [TestCase("")]
        public void test_boicot_congressman_denies_law(string law)
        {
            var congressmans = new CongressmanBase[]
            {
                new Congressman(),
                new Congressman(),
                new BoicotCongressman()
            };

            var lawStatement = WhenVoting(congressmans, law);

            ThenLawStatementIsEqualTo(lawStatement.Value, LawStatement.Denied);
        }
        
        [TestCase("long aswer")]
        [TestCase("short")]
        [TestCase("")]
        public void test_cometa_congressman_denies_law(string law)
        {
            var congressmans = new CongressmanBase[]
            {
                new Congressman(),
                new CometaCongressman(COMETA),
                new CometaCongressman(COMETA)
            };

            var lawStatement = WhenVoting(congressmans, law);

            ThenLawStatementIsEqualTo(lawStatement.Value, LawStatement.Approved);
        }
        
        [TestCase("long answer")]
        public void test_anxious_congressman_denies_law(string law)
        {
            var congressmans = new CongressmanBase[]
            {
                new Congressman(),
                new AnxiousCongressman(),
                new AnxiousCongressman()
            };

            var lawStatement = WhenVoting(congressmans, law);

            ThenLawStatementIsEqualTo(lawStatement.Value, LawStatement.Denied);
        }

        private Maybe<LawStatement> WhenVoting(CongressmanBase[] congressmans, string law)
        {
            var lawStatement = Maybe<LawStatement>.Nothing;
            _congressmanCamera.Vote(congressmans, law).Subscribe(statement => lawStatement = statement.ToMaybe());
            testScheduler.Start();
            return lawStatement;
        }

        private void ThenLawStatementIsEqualTo(LawStatement lawStatement, LawStatement expected)
        {
            Assert.AreEqual(expected, lawStatement);
        }
    }
}