using Functional.Maybe;
using NUnit.Framework;
using UniRx;

namespace Editor
{
    public class CometaCongressmanTest
    {
        private const bool COMETABLE = true;
        private const VoteType APPROVED = VoteType.Approved;
        private const VoteType DENIED = VoteType.Denied;
        private const string SHORT_LAW = "bad";
        
        private CometaCongressman _congressman;
        private TestScheduler _testScheduler;

        [SetUp]
        public void SetUp()
        {
            _testScheduler = new TestScheduler();
            Scheduler.DefaultSchedulers.TimeBasedOperations = _testScheduler;
            
        }
        
        [Test]
        public void approve_law_when_is_a_short_law_and_is_cometable()
        {
            GivenACometableCongressman();
            var vote = WhenVoting(SHORT_LAW);

            ThenVoteIsEqualTo(vote.Value, APPROVED);
        }
        
        [Test]
        public void denied_law_when_is_a_short_law_and_isnt_cometable()
        {
            GivenANotCometableCongressman();
            var vote = WhenVoting(SHORT_LAW);

            ThenVoteIsEqualTo(vote.Value, DENIED);
        }

        private void GivenACometableCongressman()
        {
            _congressman = new CometaCongressman(COMETABLE);
        }
        
        private void GivenANotCometableCongressman()
        {
            _congressman = new CometaCongressman();
        }

        Maybe<VoteType> WhenVoting(string law)
        {
            Maybe<VoteType> vote = default;
            _congressman.Vote(law).Subscribe(currentVote => vote = currentVote.ToMaybe());
            _testScheduler.Start();
            return vote;
        }

        private void ThenVoteIsEqualTo(VoteType current, VoteType expected)
        {
            Assert.AreEqual(expected, current);
        }
    }
}